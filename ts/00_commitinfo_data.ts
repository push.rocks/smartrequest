/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartrequest',
  version: '2.0.18',
  description: 'dropin replacement for request'
}

// this file implements methods to get and post binary data.
import * as interfaces from './smartrequest.interfaces.js';
import { request } from './smartrequest.request.js';

import * as plugins from './smartrequest.plugins.js';

export const getBinary = async (
  domainArg: string,
  optionsArg: interfaces.ISmartRequestOptions = {}
) => {
  optionsArg = {
    ...optionsArg,
    autoJsonParse: false,
  };
  const done = plugins.smartpromise.defer();
  const response = await request(domainArg, optionsArg, true);
  const data: Array<Buffer> = [];

  response
    .on('data', function (chunk: Buffer) {
      data.push(chunk);
    })
    .on('end', function () {
      //at this point data is an array of Buffers
      //so Buffer.concat() can make us a new Buffer
      //of all of them together
      const buffer = Buffer.concat(data);
      response.body = buffer;
      done.resolve();
    });
  await done.promise;
  return response;
};

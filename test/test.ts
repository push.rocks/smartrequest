import { tap, expect, expectAsync } from '@pushrocks/tapbundle';

import * as smartrequest from '../ts/index.js';

tap.test('should request a html document over https', async () => {
  await expectAsync(smartrequest.getJson('https://encrypted.google.com/')).toHaveProperty('body');
});

tap.test('should request a JSON document over https', async () => {
  await expectAsync(smartrequest.getJson('https://jsonplaceholder.typicode.com/posts/1'))
    .property('body')
    .property('id')
    .toEqual(1);
});

tap.test('should post a JSON document over http', async () => {
  await expectAsync(smartrequest.postJson('http://md5.jsontest.com/?text=example_text'))
    .property('body')
    .property('md5')
    .toEqual('fa4c6baa0812e5b5c80ed8885e55a8a6');
});

tap.test('should safe get stuff', async () => {
  smartrequest.safeGet('http://coffee.link/');
  smartrequest.safeGet('https://coffee.link/');
});

tap.skip.test('should deal with unix socks', async () => {
  const socketResponse = await smartrequest.request(
    'http://unix:/var/run/docker.sock:/containers/json',
    {
      headers: {
        'Content-Type': 'application/json',
        Host: 'docker.sock',
      },
    }
  );
  console.log(socketResponse.body);
});

tap.skip.test('should correctly upload a file using formData', async () => {});

tap.start();
